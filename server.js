var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

//Agregado para que se vea en browser y tome el index del build
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('Proyecto polymer con wrepper en NodeJS en puerto: ' + port);

app.get('/',function(req, res){
  //res.sendFile(path.join(__dirname, 'index.html')); //index local
  res.sendFile('index.html', {root: '.'}); //index del build (release)
})
